#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h> 
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>

typedef struct {
	char name[51];
   	char email[51];
	char phone[12];
	char power[51];
	char time[20];
	int done;
	time_t time_msec;
} Customer;

typedef struct node {
    Customer cus;
    struct node *next;
} node_t;

	// Adat bevitel
void new_customer(node_t **head) {
    Customer new_cus;
    time_t now;
    char c;

    while((c = getchar()) != '\n' );

    	printf("Nev (50 karakter): ");//nev
    	scanf("%[^\n]%*c", new_cus.name);
    	printf("E-mail (50 karakter): ");//e-mail
    	scanf("%s", new_cus.email);
    	printf("Telefon szam (11 karakter): ");//telefonszam
    	scanf("%s", new_cus.phone);
		printf("Teljesitmeny igeny (Watt): ");//teljesitmeny
		scanf("%s", &new_cus.power);
	
    now = time(0);
    strftime (new_cus.time, 100, "%Y-%m-%d %H:%M:%S", localtime(&now)); //rendszerido
	new_cus.time_msec=now;
	new_cus.done=0;

    if(*head == NULL) {
        *head = malloc(sizeof(node_t));
        (*head)->cus = new_cus;
        (*head)->next = NULL;
    } else {
    	node_t *current = *head;

    	while(current->next != NULL) {
        	current = current->next;
    	}

    	current->next = malloc(sizeof(node_t));
    	current->next->cus = new_cus;
    	current->next->next = NULL;
    }
}

	// Teljes listazas
void list_all(node_t *head) {
    node_t *current = head;

    if(head == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

    while(current != NULL) {
        printf("\n%s\n%s\n%s\n%s\n%s\n",current->cus.name, current->cus.email, current->cus.phone, current->cus.power, current->cus.time);
        current = current->next;
    }
}

  // Listazas ugyfel alapjan
void list_customer(node_t *head){
	node_t *current = head;
	char n[51];
	char c;

    if(head == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

	while((c = getchar()) != '\n' );

	printf("Nev:\n");
	scanf("%[^\n]%*c",n);

	while(current != NULL){
		if(strcmp(current->cus.name, n) == 0) {
            printf("\n%s\n%s\n%s\n%s\n%s\n",current->cus.name, current->cus.email, current->cus.phone,current->cus.power, current->cus.time);

        }
        	current = current->next;
      }
}

	// Listazas teljesitmeny igenyre
void list_power(node_t *head){
	node_t *current = head;
	char n[5];
	char c;

    if(head == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

	while((c = getchar()) != '\n' );

	printf("Teljesitmeny:\n");
	scanf("%s",n);

	while(current != NULL){
		if(strcmp(current->cus.power, n) == 0) {
            printf("\n%s\n%s\n%s\n%s\n%s\n",current->cus.name, current->cus.email, current->cus.phone,current->cus.power, current->cus.time);

        }
        	current = current->next;
      }

}

	// Kereses a torleshez--index
int search_node(node_t *head, const char *n) {
    node_t *current = head;
    int i = 0;

    if(head == NULL) {
        return -1;
    }
    while(current != NULL && (strcmp(current->cus.name, n) != 0)) {
        current = current->next;
        i++;
    }
    if(current == NULL) {
        return -1;
    }else{
		return i;
    }
}

	// Torles nev alapjan
void remove_by_name(node_t **head) {
    node_t *temp_node = NULL;
    node_t *current = *head;
    char n[51];
    int i = 0;
    int id;
    char c;

    if(*head == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

    while((c = getchar()) != '\n');

    printf("Nev: ");
    scanf("%[^\n]%*c", n);

    id = search_node(*head, n);//kereses

    if(id == -1) {
        printf("Nincsenek adatok!\n");
        return;
    } else if(id == 0) {
		node_t *next_node = NULL;
    	next_node = (*head)->next;
    	free(*head);
    	*head = next_node;
    } else {
        for(i = 0; i < id-1; ++i) {
            current = current->next;
        }
        temp_node = current->next;
        current->next = temp_node->next;
        free(temp_node);
    }
}

	// Modositas
void modify_customer(node_t **head) {
    node_t *current = *head;
    Customer mod_cus;
    char n[51];
    char c;

    if(*head == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

    while((c = getchar()) != '\n');	

    printf("Nev: ");
    scanf("%[^\n]%*c", n);

    while(current != NULL && (strcmp(current->cus.name, n) != 0)) {
        current = current->next;
    }

    if(current == NULL) {
        printf("Nincsenek adatok!\n");
        return;
    }

    	printf("Uj nev (50 karakter): ");
    	scanf("%[^\n]%*c", mod_cus.name);
    	printf("Uj e-mail (50 karakter): ");
    	scanf("%s", mod_cus.email);
		printf("Uj telefonszam (11 karakter): ");
		scanf("%s", mod_cus.phone);
    	printf("Uj teljesitmeny igeny (Watt): ");
    	scanf("%s", mod_cus.power);



    strcpy(mod_cus.time, current->cus.time);
    current->cus = mod_cus;
}

void handler_receipt(int signum) {
	printf("NYUGTA!\n");
}
void handler_signal2(int signum) {
	printf("1. JELZES!\n");
}
void handler_signal1(int signum) {
	printf("2. JELZES!\n");
}

void work(node_t *head){
	//AZ OSSZES MEGRENDELES
	node_t *current = head;
	if(head == NULL) {
		printf("Nincsenek megrendelesek!\n");
		return;
	}
	Customer customers[50];//osszes vasarlo megrendeles
	int customer_count = 0;
	while (current != NULL) {
		customers[customer_count] = current->cus;
		customer_count++;
		current = current->next;
	}

	int pipe_job[2];
	int pipe_job_done[2];
	if (pipe(pipe_job) == -1) {printf("Error opening pipe\n");}
	if (pipe(pipe_job_done) == -1) {printf("Error opening pipe\n");}

	signal(SIGTERM, handler_receipt);
	signal(SIGUSR1, handler_signal1);
	signal(SIGUSR2, handler_signal2);

	pid_t child=fork(); 
	if (child<0){perror("A gyerek folyamat letrehozasa sikertelen volt.\n"); exit(1);} 
	if (child>0) {//szulo folyamat
		close(pipe_job[0]);
		close(pipe_job_done[1]);

		//AMIG EL NEM KESZULUNK AZ OSSZES MEGRENDELESSEL
		int all_done=0;
		while(all_done == 0){
			all_done=1;
			int i;
			for(i=0;i<customer_count;i++){
				//HA MEG NINCS KESZ EGY MEGRENDELES
				if(customers[i].done == 0){
					all_done = 0;
					Customer job[2];
					int j;
					int have_paar = 0;
					//KERESUNK NEKI EGY PART
					for (j = i+1; j < customer_count;j++)
					{
						if(strcmp(customers[i].power,customers[j].power) == 0){
							job[0] = customers[i];
							job[1] = customers[j];
							have_paar = 1;
							break;
						}
					}
					time_t now = time(0);
					//HA VAN PARJA VAGY HA NINCS PARJA, DE ELTELT EGY HET: OK
					if(have_paar == 1){
						printf("SZULO: A mai munka: %s, %s szamara a rendeles telepitese.(Elkuldom csovezeteken)\n"
						,customers[i].name,customers[j].name);
						
						write(pipe_job[1],&have_paar,sizeof(have_paar));
						write(pipe_job[1],&job[0],sizeof(Customer));
						write(pipe_job[1],&job[1],sizeof(Customer));

						sleep(1);
						//printf("SZULO: Fogadtam a nyugtat, hogy megerkeztek a rendelesek reszletei.\n");
						
						sleep(1);
						//printf("SZULO: Fogadtam a jelzest az elso teljesitett feladatrol.\n");
						
						sleep(1);
						//printf("SZULO: Fogadtam a jelzest a masodik teljesitett feladatrol.\n");
						
						char s[5];
						read(pipe_job_done[0],s,5);
						//printf("SZULO: Fogadtam az uzenetet a csovezeteken, hogy a napi munkak keszen vannak: %s\n",s);

						customers[i].done = 1;
						customers[j].done = 1;
						break;
					}else if((now - customers[i].time_msec) >= 7 ){
						job[0] = customers[i];
						printf("SZULO: A mai munka: %s szamara a rendeles telepitese.(Elkulom csovezeteken)\n",customers[i].name);
						
						write(pipe_job[1],&have_paar,sizeof(have_paar));
						write(pipe_job[1],&job[0],sizeof(Customer));
						
						sleep(1);
						//printf("SZULO: Fogadtam a nyugtat, hogy megerkeztek a rendelesek reszletei.\n");	
						
						sleep(1);
						//printf("SZULO: Fogadtam a jelzest a teljesitett feladatrol.\n");
						
						char s[5];
						read(pipe_job_done[0],s,5);
						//printf("SZULO: Fogadtam az uzenetet a csovezeteken, hogy a napi munka keszen van.\n");

						customers[i].done = 1;
						break;
					}
				}
			}
		}
		printf("SZULO: Az osszes megrendeles keszen van!\n");
		close(pipe_job_done[0]);
		close(pipe_job[1]);

		int status;
		waitpid(child, &status, 0);
		
	}else{//gyerek folyamat	
		close(pipe_job[1]);
		close(pipe_job_done[0]);
		
		Customer job[2];
		int have_paar;

		while(read(pipe_job[0],&have_paar,sizeof(have_paar)) != -1 && have_paar != -1)
		{	
			if(have_paar == 1){
				read(pipe_job[0],&job[0],sizeof(Customer));
				read(pipe_job[0],&job[1],sizeof(Customer));
				printf("GYEREK: A mai munka: %s, %s szamara a rendeles telepitese.(Megkaptam csovezeteken)\n"
				,job[0].name,job[1].name);
				
				//printf("GYEREK: Elkuldom a nyugtat, hogy megerkeztek a rendelesek reszletei.\n");
				kill(getppid(),SIGTERM);
				
				//printf("GYEREK: Elkuldom a jelzest, az elso teljesitett feladatrol.\n");
				kill(getppid(),SIGUSR2); 
				
				//printf("GYEREK: Elkuldom a jelzest, a masodik teljesitett feladatrol.\n");
				kill(getppid(),SIGUSR1); 
				
				sleep(1);//eltelik egy nap
				//printf("GYEREK: Elkuldom az uzenetet a csovezeteken, hogy a napi munka keszen van.\n");
				write(pipe_job_done[1],"KESZ",5);
			}else{
				read(pipe_job[0],&job[0],sizeof(Customer));
				printf("GYEREK: A mai munka: %s szamara a rendeles telepitese.(Megkaptam csovezeteken)\n"
				,job[0].name);
				
				//printf("GYEREK: Elkuldom a nyugtat, hogy megerkeztek a rendelesek reszletei.\n");
				kill(getppid(),SIGTERM); 
				
				//printf("GYEREK: Elkuldom a jelzest, a teljesitett feladatrol.\n");
				kill(getppid(),SIGUSR2); 
				
				sleep(1);//eltelik egy nap
				//printf("GYEREK: Elkuldtom az uzenetet a csovezeteken, hogy a napi munka keszen van.\n");
				write(pipe_job_done[1],"KESZ",5);
				
			}
			have_paar = -1;
		}
		close(pipe_job[0]);
		close(pipe_job_done[1]);
		exit(0);
		
	}
}

	// Fajlba iras
void write_to_file(node_t *head) {
    node_t *current = head;
    FILE *file;

    if(head == NULL) {
        remove("out.txt");
        return;
    }

    file = fopen("out.txt", "w");

    if(file == NULL) {
        printf("Hiba a fajl megnyitasa kozben!\n");
        exit(1);
    }

    while(current != NULL) {
        fprintf(file,"%s\t%s\t%s\t%s\t%s\n",current->cus.name, current->cus.email, current->cus.phone,current->cus.power, current->cus.time);
        current = current->next;
    }
    fclose(file);
}

int main() {
    node_t *head = NULL;
    char c;
	//Menu
    do {
        printf("1.)Adatok bevitele\n"
               "2.)Teljes listazas\n"
               "3.)Listazas ugyfel alapjan\n"
			   "4.)Listazas teljesitmeny igenyre\n"
               "5.)Modositas\n"
               "6.)Torles nev alapjan\n"
			   "7.)Munka inditasa\n"
               "0.)Kilepes\n");
        scanf(" %c", &c);
	
        switch(c) {
            case '1': {
                new_customer(&head);
                printf("\n");
				write_to_file(head);
                break;
            }
            case '2': {
                list_all(head);
                printf("\n");
                break;
            }
            case '3': {
                list_customer(head);
                printf("\n");
                break;
            }
			
            case '4': {
				list_power(head);
                printf("\n");
                break;
            }
			
            case '5': {
                modify_customer(&head);
                printf("\n");
				write_to_file(head);
                break;
            }
            case '6': {
                remove_by_name(&head);
                printf("\n");
				write_to_file(head);
                break;
            }
			case '7': {
                work(head);
                printf("\n");
                break;
            }
			case '0': {
				break;
			}
			default: {
				printf("\nKerem a felsorolt lehetosegek kozul valasszon!\n\n");
			}
        }
    } while(c != '0');

	//felszabaditas!!!
    while (head != NULL) {
        node_t* temp = head;
        head = head->next;
        free(temp);
    }

    return 0;
}