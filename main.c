#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h> 
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>


/*
Outer sources for my linked list impl:
https://www.learn-c.org/en/Linked_lists
https://www.geeksforgeeks.org/modify-contents-linked-list/
https://www.geeksforgeeks.org/linked-list-set-3-deleting-node/
https://stackoverflow.com/questions/11573974/write-to-txt-file
*/

typedef struct {
	char name[31];
   	char email[31];
	char phone[12];
	int power;
	time_t time;
	int complete;
} Customer;

struct Node {
    Customer customer;
    struct Node *next;
};

void pushCustomer(struct Node** head_ref) {
	struct Node* tmp;
    Customer customer;
    char c;
	
    while((c = getchar()) != '\n' );
	printf("Name: ");
    scanf("%s", customer.name);
    printf("E-mail: ");
    scanf("%s", customer.email);
    printf("Phone: ");
    scanf("%s", customer.phone);
	printf("Power claim: ");
	scanf("%i", &customer.power);
	time_t actTime;
	time(&actTime);
	customer.time = actTime;
	customer.complete = 0;
    if(*head_ref == NULL) {
        *head_ref = malloc(sizeof(struct Node));
        (*head_ref)->customer = customer;
        (*head_ref)->next = NULL;
    } else {
    	struct Node *current = *head_ref;
    	while(current->next != NULL) {
        	current = current->next;
    	}
    	current->next = malloc(sizeof(struct Node));
    	current->next->customer = customer;
    	current->next->next = NULL;
    }
}

void listAll(struct Node *head) {
    struct Node *current = head;
    if(head != NULL) {
        while(current != NULL) {
			char timenow[20];
			strftime (timenow, 100, "%Y.%m.%d %H:%M:%S", localtime(&current->customer.time));
			printf("\n%s\n%s\n%s\n%i\n%s\n%i\n",current->customer.name, current->customer.email, current->customer.phone, current->customer.power, timenow, current->customer.complete);
			current = current->next;
		}
	}else{
		printf("No data exists!\n");
        return;
	}
}

void listByCustomer(struct Node *head){
	struct Node *current = head;
	char name[31];
	char c;
    if(head != NULL) {
        while((c = getchar()) != '\n' );
		printf("Name: \n");
		scanf("%s",name);
		while(current != NULL){
			if(strcmp(current->customer.name, name) == 0) {
				char timenow[20];
				strftime (timenow, 100, "%Y.%m.%d %H:%M:%S", localtime(&current->customer.time));
				printf("\n%s\n%s\n%s\n%i\n%s\n", current->customer.name, current->customer.email, current->customer.phone, current->customer.power, timenow);
			}
			current = current->next;
		  }
    }else{
		printf("No data exists\n");
        return;
	}
}

void listByPowerClaim(struct Node *head){
	struct Node *current = head;
	char c;
	int inputPower;
    if(head != NULL) {
        while((c = getchar()) != '\n' );
		printf("Power: \n");
		scanf("%i",&inputPower);
		while(current != NULL){
			if(current->customer.power == inputPower) {
				char timenow[20];
				strftime (timenow, 100, "%Y.%m.%d %H:%M:%S", localtime(&current->customer.time));
				printf("\n%s\n%s\n%s\n%i\n%s\n", current->customer.name, current->customer.email, current->customer.phone, current->customer.power, timenow);
			}
			current = current->next;
      }
    }else{
		printf("No data exists! \n");
        return;
	}
}

void removeByName(struct Node **head) {
    struct Node* temp = *head, *prev;
	char key[31];
	char c;
	
	while((c = getchar()) != '\n');
		printf("Name: ");
		scanf("%s", key);
    if (temp != NULL && strcmp(temp->customer.name, key) == 0) 
    { 
        *head = temp->next;
        free(temp);
        printf("Remove succesfull");
		return; 
    } 
    while (temp != NULL && strcmp(temp->customer.name,key) != 0) 
    { 
        prev = temp; 
        temp = temp->next; 
    } 
    if (temp == NULL){
		printf("No key found");
		return; 
	} 
    prev->next = temp->next; 
    free(temp);
	printf("remove succesfull");
}
//it was written in case of removing a task from the list when its already complete.
void removeByName2(char *name, struct Node** head)
{
	struct Node* temp = *head, *prev;
	char *key = name;
	
	if (temp != NULL && strcmp(temp->customer.name, key) == 0) 
    { 
        *head = temp->next;
        free(temp);
        printf("Task complete");
		return; 
    } 
    while (temp != NULL && strcmp(temp->customer.name,key) != 0) 
    { 
        prev = temp;
        temp = temp->next; 
    } 
    if (temp == NULL){
		printf("No key found");
		return; 
	} 
    prev->next = temp->next; 
    free(temp);
	printf("Task complete");
}


void modifyByName(struct Node **head) {
    struct Node *current = *head;
    Customer modifiedCustomer;
    char name[31];
    char c;

    if(*head == NULL) {
        printf("No data exists\n");
        return;
    }
    while((c = getchar()) != '\n');	
    printf("Name: ");
    scanf("%s", name);
    while(current != NULL && (strcmp(current->customer.name, name) != 0)) {
        current = current->next;
    }
    if(current != NULL) {
		printf(	"1 - Modify name\n"
				"2 - Modify e-mail\n"
				"3 - Modify phone\n"
				"4 - Modify power\n");
		scanf(" %c", &c);
        switch(c) {
            case '1': {
                printf("Name: ");
				scanf("%s", modifiedCustomer.name);
				strcpy(modifiedCustomer.email,current->customer.email);
				strcpy(modifiedCustomer.phone,current->customer.phone);
				modifiedCustomer.power = current->customer.power;
                break;
            }
			case '2': {
                printf("E-mail: ");
				scanf("%s", modifiedCustomer.email);
				strcpy(modifiedCustomer.name,current->customer.name);
				strcpy(modifiedCustomer.phone,current->customer.phone);
				modifiedCustomer.power = current->customer.power;
                break;
            }
			case '3': {
                printf("Phone: ");
				scanf("%s", modifiedCustomer.phone);
				strcpy(modifiedCustomer.name,current->customer.name);
				strcpy(modifiedCustomer.email,current->customer.email);
				modifiedCustomer.power = current->customer.power;
                break;
            }
			case '4': {
                printf("Power: ");
				scanf("%i", &modifiedCustomer.power);
				strcpy(modifiedCustomer.name,current->customer.name);
				strcpy(modifiedCustomer.email,current->customer.email);
				strcpy(modifiedCustomer.phone,current->customer.phone);
                break;
            }	
			default:{
				printf("default");
			}
		}
	}else{
		return;
	}
   // strcpy(modifiedCustomer.time, current->customer.time);
    modifiedCustomer.time = current->customer.time;
	current->customer = modifiedCustomer;
}

void insertDatasToFile(struct Node* head) {
    struct Node *current = head;
    FILE *f;
	if(head != NULL){
		f = fopen("data.txt", "w");

		if(f == NULL) {
			printf("Error occured while opening file!\n");
			exit(1);
		}
		while(current != NULL) {
			char timenow[20];
			strftime (timenow, 100, "%Y.%m.%d %H:%M:%S", localtime(&current->customer.time));
			fprintf(f,"%s\t%s\t%s\t%i\t%s\n",current->customer.name, current->customer.email, current->customer.phone,current->customer.power, timenow);
			current = current->next;
		}
		fclose(f);
	} else {
		printf("No data exists\n");
		return;
	}
}

void info(){
	printf("Typically for one year 1000KWh energy 1KW performance 4 solar panels panel ad!");
}

void handler(int recv)
{
	printf("1 Task complete\n");
}


void handler2(int signum)
{
	printf("2 Tasks complete!\n");
}

void taskSend(struct Node **head)
{
	//
	pid_t workers;
	struct Node *customers = *head;
	int complete = 0;
	
	
	if(head == NULL) {
		printf("No data exist!\n");
		return;
		//break;
	}
	
    int fd1[2]; 
	int fd2[2]; 
	int fd3[3];
	if (pipe(fd1) == -1)
    {
		printf("Pipe failed \n");
    }
    if (pipe(fd2) == -1)
    {
		printf("Pipe failed \n");
    }
    if(pipe(fd3) == -1)
    {
        printf("Pipe failed \n");
    }

	signal(SIGUSR1, handler);
	signal(SIGTERM, handler2);
	
	workers = fork();
	if(workers < 0)
	{
		printf("Fork failed.\n"); 
		return;
	}
	
	if(workers > 0)
	{
	    //manager
		close(fd1[0]);
		close(fd2[1]);
		close(fd3[1]);
		if(customers == NULL)
		{
			printf("No task TODO!\n");
			return;
		}
		while(customers != NULL)
		{
		    printf("customers: %s\n",customers->customer.name);
			int found = 0;
			//ha a soron következő nincs kész
			if(customers->customer.complete == 0)
			{
				//ha a soron következő nincs kész, de van még elem akkor megnézi hogy van e még ilyen tipusu feladt
				struct Node *customers2 = customers->next;
				while(customers2 != NULL && found == 0)
				{	
					if(customers2->customer.power == customers->customer.power)
					{
					    printf("Found customer with same power claim!\n");
						found = 1;
					} else 
					{
						customers2 = customers2->next;
					}
				}
				
				//ha volt iylen feladat 
				if(found == 1){
					int dif = difftime (customers2->customer.time,customers->customer.time);
					if(dif > 7)
					{
					    int in_time = 0;
						write(fd1[1],&in_time,sizeof(int));
						write(fd1[1],&customers->customer, sizeof(Customer));
			            
			            char recv[14];
			            read(fd3[0], &recv, sizeof(recv));
			    //        printf("MANAGER: %s\n",recv);
			            
			            sleep(1);
			            
						int is_complete;
						read(fd2[0], &is_complete, sizeof(int));
						customers->customer.complete = 1;
					} else
					{
						write(fd1[1],&found,sizeof(found));
						write(fd1[1],&customers->customer,sizeof(Customer));
						write(fd1[1],&customers2->customer,sizeof(Customer));
			            
			            char recv[14];
			            read(fd3[0], &recv, sizeof(recv));
			     //       printf("MANAGER: %s\n",recv);
			            
			            sleep(1);
			            sleep(1);
			            
						int is_complete;
						read(fd2[0], &is_complete, sizeof(int));

						customers->customer.complete = 1;
						customers2->customer.complete = 1;
					}
				} else 
				{
					write(fd1[1],&found,sizeof(found));
					write(fd1[1],&customers->customer,sizeof(Customer));
				
		            char recv[14];
	                read(fd3[0], &recv, sizeof(recv));
	       //         printf("MANAGER: %s\n",recv);
                    
                    sleep(1);
                    
					int is_complete;
					read(fd2[0], &is_complete, sizeof(int));
				//	printf("MANAGER: %i\n",is_complete);
					customers->customer.complete = 1;
				}
			}
			customers = customers->next;
		}
		//close(fd);
		close(fd1[1]);
		close(fd2[0]);
		close(fd3[0]);
		wait(NULL);
		printf("All Task Complete!!\n");

        //exit
        return;
	} else {
		//workers

		int tasknum;
		close(fd1[1]);
		close(fd2[0]);
		close(fd3[0]);
		while(read(fd1[0],&tasknum,sizeof(tasknum)) != -1 && tasknum != -1)
		{
    	    Customer task1;
		    Customer task2;
		    printf("tasknum: %i\n", tasknum);
			if(tasknum == 1)
			{
				//task complete
				read(fd1[0],&task1,sizeof(Customer));
				read(fd1[0],&task2,sizeof(Customer));
                
				write(fd3[1],"Task Recieved!",15);
				
				kill(getppid(), SIGUSR1);
				kill(getppid(), SIGTERM);
				
        		int is_complete = 1;		
				write(fd2[1], &is_complete, sizeof(int));
			} else if(tasknum == 0)
			{
				read(fd1[0], &task1, sizeof(Customer));
	
				write(fd3[1],"Task Recieved!",15);
				
				kill(getppid(), SIGUSR1);
				
				int is_complete = 1;
				write(fd2[1], &is_complete, sizeof(int));
			}
			tasknum = -1;
		}
		close(fd1[0]);
		close(fd2[1]);
		close(fd3[1]);
    	exit(0);
	}
}

int main() {
    struct Node *head = NULL;
    char c;
    do {
        printf("1 - Data insert\n"
               "2 - List all\n"
               "3 - List by customer name\n"
			   "4 - List by power claim \n"
               "5 - Modify\n"
               "6 - Delete\n"
			   "7 - Help \n"
			   "8 - Send tasks to workers \n"
               "0 - Quit\n");
        scanf(" %c", &c);
        switch(c) {
            case '1': {
                pushCustomer(&head);
				insertDatasToFile(head);
                printf("\n");
                break;
            }
            case '2': {
                listAll(head);
                printf("\n");
                break;
            }
            case '3': {
                listByCustomer(head);
                printf("\n");
                break;
            }
            case '4': {
				listByPowerClaim(head);
                printf("\n");
                break;
            }
            case '5': {
                modifyByName(&head);
				insertDatasToFile(head);
                printf("\n");
                break;
            }
            case '6': {
				removeByName(&head);
				insertDatasToFile(head);
				printf("\n");
                break;
            }
			case '7': {
				info();
				break;
			}
			case '8': {
				taskSend(&head);
				break;
			}
			case '0': {
				break;
			}
			default: {
				printf("\nPlease choose only from (0 - 7) menu options!\n\n");
			}
        }
		
    } while(c != '0');
    while (head != NULL) {
        struct Node* temp = head;
        head = head->next;
        free(temp);
    }
    return 0;
}
