#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h> 
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/msg.h> 

/*
3-as 4-es jegyig
*/

struct uzenet { 
     long mtype;//ez egy szabadon hasznalhato ertek, pl uzenetek osztalyozasara
     char mtext [ 1024 ]; 
}; 

void handler_jancsi(int signum)
{
	printf("JANCSI: KOPP KOPP(JELZES)\n");		
}

void handler_juliska(int signum)
{
	printf("JULISKA: KOPP KOPP(JELZES)\n");	
}
void handler_jelzes(int signum)
{
	printf("JULISKA: JELZES BANYANAK\n");
}
void handler_eves(int signum)
{
	//nem ir ki semmit csak jelez jancsinak h zabĂˇlhat
	
}
void handler_jelzes2(int signum)
{
	//ez se ir ki semmit csak jelez
}

int main(int argc, char* argv[])
{
	pid_t jancsi;
	pid_t juliska;
	key_t key; 
    int msgid; 
	
	int fd1[2];
	int fd2[2];
	char cookies[2][10]= {"kalacs", "sutemeny"};
	int jancsis_thumb[3] = {-1, 0, 1};
	
	if(pipe(fd1) == -1)
	{
		perror("Hiba a pipe nyitaskor!");
	    exit(EXIT_FAILURE);
	}
	if(pipe(fd2) == -1)
	{
        perror("Hiba a pipe nyitaskor!");
	    exit(EXIT_FAILURE);
	}
	
	signal(SIGUSR1, handler_jancsi);
	signal(SIGUSR2, handler_juliska);
	signal(SIGTERM, handler_jelzes);
	signal(SIGHUP, handler_eves);
	signal(SIGKILL, handler_jelzes2);
	
	key = ftok("mintazh.c", 1);
	msgid = msgget(key, 0666 | IPC_CREAT); 
	if(msgid < 0)
	{
		perror("Failed to create IPC");
	}
	juliska = fork();
	if(juliska < 0)
	{
		perror("Fork failed at Jancsi\n");
	}
	if(juliska > 0)
	{
        pause();
		//VASORRU GECI
		jancsi = fork();
		if(jancsi < 0)
		{
			perror("Fork failed at Juliska\n");
		}
		if(jancsi > 0)
		{
			//still VASORRU GECI
			pause();
		    srand(time(NULL));
			int ind = rand() % 2;
            write(fd2[1], cookies[ind], sizeof(cookies[ind]));
			sleep(2);
			
			int pid;
			read(fd2[0], &pid, sizeof(int));
			kill(pid, SIGHUP);
			sleep(1);
			
	//		kill(pid, SIGHUP);
	//		sleep(1);
			wait(NULL);
		} else 
		{
			//jancsi session
			kill(getppid(), SIGUSR2);
			sleep(2);
			char* cookie;
			read(fd2[0], &cookie, 11 );
			printf("Jancsi kapott: %s \n",&cookie);
			int pid = getpid();
			write(fd2[1], &pid, sizeof(int));
			sleep(10);
		//	pause();
			printf("MEHET A KAJA\n");
			sleep(1);			
			//eszik 3 sec-enkĂ©nt
			printf("Jancsi ZABAL\n");
			sleep(1);
			printf("Jancsi ZABAL\n");
			sleep(1);
			printf("Jancsi ZABAL\n");
			sleep(1);
			
			
			int ind2 = rand() % 2;
			write(fd2[1], &jancsis_thumb[ind2], sizeof(int));

			sleep(3);
			const struct uzenet uz = { 5, "ZARD BE A BOSZIT A FASKAMRABA!" };
			int status;
			status = msgsnd(msgid,&uz,strlen(uz.mtext) + 1,0);
			if(status < 0)
			{
				perror("msgsnd error\n");
				
			}
			sleep(2);
			exit(0);
		}
//VASUORRU
		srand(getpid());
		int ind = rand() % 2;
        write(fd1[1], cookies[ind], sizeof(cookies[ind]));
		//int pid;
		//read(fd[2], &pid, sizeof(int));
		//kill(pid,SIGHUP)
		pause();
		char banya_ir[] = "banya utasitasa 1";
		write(fd1[1], banya_ir, strlen(banya_ir) + 1);
		printf("BANYA: %s\n",banya_ir);
		pause();
		char banya_ir2[] = "banya utasitasa 2";
		write(fd1[1], banya_ir2, strlen(banya_ir2) + 1);
		printf("BANYA: %s\n",banya_ir2);
		pause();
		char banya_ir3[] = "banya utasitasa 3";
		write(fd1[1], banya_ir3, strlen(banya_ir3) + 1);
		printf("BANYA: %s\n",banya_ir3);
		
		sleep(1);
		int* thumb;
		read(fd2[0],&thumb, sizeof(int));
		printf("VASORRU KONSTATALJA HOGY JANCSI UJJANAK EREDMENYE: %i\n", thumb);
		sleep(1);
	//	msgid = msgget(key, 0666 | IPC_CREAT);
		wait(NULL);
	//IDĂIG VASORRU
	} else {
		//juliska session
		kill(getppid(), SIGUSR1);
		sleep(2);
		
		char* cookie;
		read(fd1[0], &cookie, 11 );
		printf("Juliska kapott: %s \n", &cookie);
		printf("Bent vannak a hazba!\n");
	//	int pid = getpid();
		//write(fd1[1], &pid, sizeof(int));
		
	//	pause();
		//jelzĂ©s banyĂˇnak mĂˇsodpercenkĂ©nt 3 alkalommal
		sleep(1);
		kill(getppid(), SIGTERM);
		sleep(1);
		kill(getppid(), SIGTERM);
		sleep(1);
		kill(getppid(), SIGTERM);
		sleep(4);
		
		
		struct uzenet uz2;
		int status;
		status = msgrcv(msgid, &uz2, 1024, 5, 0);
		if(status < 0)
		{
			perror("msgrcv error"); 
		}
		sleep(3);
		printf("UZENETSOR JANCSITOL: %s\n", uz2.mtext);
		sleep(1);
		printf("JULISKA: BEZARTAM\n");
		sleep(1);
		
		exit(0);
	}
//	sleep(3);
    close(fd1[1]);
	close(fd1[0]);
	close(fd2[1]);
    close(fd2[0]);
	msgctl(msgid, IPC_RMID, NULL); 
	return 0;
}