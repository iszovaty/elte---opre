#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h> // for errno, the number of last error

int main(int argc, char* argv[])
{
    int pid,fd,fd2;
    printf("Fifo start!\n");
    int fid=mkfifo("fifo.ftc", S_IRUSR|S_IWUSR ); // creating named pipe file
    // S_IWGRP, S_IROTH (other jog), file permission mode
    // the file name: fifo.ftc
    // the real fifo.ftc permission is: mode & ~umask 
    if (fid==-1)
    {
		printf("Error number: %i",errno);
		exit(EXIT_FAILURE);
    }
    printf("Mkfifo vege, fork indul!\n");
    pid = fork();
    
    if(pid>0)   //parent 
	{
		char s[1024]="Semmi";		
		printf("Csonyitas eredmenye: %d!\n",fid);
		fd=open("fifo.ftc",O_RDONLY);
		fd2=open("fifo.ftc",O_RDONLY);
		read(fd,s,sizeof(s));
		fd=open("fifo.ftc",O_RDONLY);
		sleep(1);
		//pause();
		printf("Ezt olvastam a csobol szulokent: %s \n",s);
		read(fd2,s,sizeof(s));
		printf("Ezt olvastam a csobol szulokent masodjara: %s \n",s);
		fd=open("fifo.ftc",O_WRONLY);
		write(fd,"Dehogy hajra!\n",13);
		printf("Szulo vagyok, valaszoltam a hajrara!\n");
		close(fd);
		close(fd2);
		wait(NULL);
		// remove fifo.ftc
		
		unlink("fifo.ftc");
    }
	else // child
	{
		char s[1024];
		printf("Gyerek vagyok, irok a csobe!\n");
		printf("Csonyitas eredmenye: %d!\n",fid);
        fd=open("fifo.ftc",O_WRONLY);
        write(fd,"Hajra Fradi!\n",12);
        fd2=open("fifo.ftc",O_WRONLY);
		write(fd2,"Hajra Fradi!\n",12);
		printf("Gyerek vagyok, beirtam, vegeztem!\n");

		
//		waitppid();
		sleep(2);
		fd=open("fifo.ftc",O_RDONLY);
		read(fd,s,sizeof(s));
		printf("Ezt olvastam a csobol gyerekkent: %s \n",s);
//		close(fd);
		
	}	
    
    return 0; 
}
