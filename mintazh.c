#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h> 
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>




void handler_jancsi(int signum)
{
	printf("JANCSI: KOPP KOPP(JELZES)\n");		
}

void handler_juliska(int signum)
{
	printf("JULISKA: KOPP KOPP(JELZES)\n");	
}

int main()
{
	pid_t jancsi;
	pid_t juliska;
	
	int fd1[2];
	int fd2[2];
	char cookies[2][10]= {"kalacs", "sutemeny"};
	
	if(pipe(fd1) == -1)
	{
		perror("Hiba a pipe nyitaskor!");
	    exit(EXIT_FAILURE);
	}
	if(pipe(fd2) == -1)
	{
        perror("Hiba a pipe nyitaskor!");
	    exit(EXIT_FAILURE);
	}
	
	signal(SIGUSR1, handler_jancsi);
	signal(SIGUSR2, handler_juliska);
	
	jancsi = fork();
	if(jancsi < 0)
	{
		perror("Fork failed at Jancsi\n");
	}
	if(jancsi > 0)
	{
        pause();
		//vas Ăłrru bĂˇba
		juliska = fork();
		if(juliska < 0)
		{
			perror("Fork failed at Juliska\n");
		}
		if(juliska > 0)
		{
			//still vasĂłrru geci
			pause();
		    srand(time(NULL));
			int ind = rand() % 2;
            write(fd2[1], cookies[ind], sizeof(cookies[ind]));
            close(fd2[1]);


			wait(NULL);
		} else 
		{
			//juliska session
			kill(getppid(), SIGUSR2);
			sleep(2);
			char* cookie;
			read(fd2[0], &cookie, 11 );
			printf("Juliska kapott: %s \n",&cookie);
            exit(0);
		}

		srand(getpid());
		int ind = rand() % 2;
        write(fd1[1], cookies[ind], sizeof(cookies[ind]));
        close(fd1[1]);
        
		wait(NULL);
		
	} else {
		//jancsi session
		kill(getppid(), SIGUSR1);
		sleep(2);
		
		char* cookie;
		read(fd1[0], &cookie, 11 );
		printf("Jancsi kapott: %s \n", &cookie);
		exit(0);
	}
//	sleep(3);
    close(fd1[0]);
    close(fd2[0]);
	printf("Bent vannak a hazba!\n");
	return 0;
}